**Quick guide**
------------

While above guide evolved, it became quite lengthy, hence here brief descriptions ("walk-throughs") of typical use cases:

[TOC]

### A. Reformatting a SDXC-card (pre-formatted with exFAT) to vFAT32
**Walk through sections 1, 2.1, (omit 2.2.a), 2.2.b, 2.3, 3.1, 3.2.a (just set partition type to "*c*"), 4.1 and 5.: Done.**

### B. Reformatting a SD-card to a Linux-native filesystem
**Walk through sections 1, 2.1, (you can omit 2.2.x), 3.1, 3.2.a (just set partition type to "*83*"), one of the subsections 4.2.x and finally 5.: Done.**

### C. Repartitioning a SD-card, deploying unencrypted and encrypted filesystems
As this is what above guide was originally developed for, there is no shortcut one can take (except for the obvious possible omissions: sections 3.2.a and 4.3.2), just a few choices to make.

Typical use cases / partitioning schemes are:

- **C.1 Create a small FAT32 partition (slightly below 8 GB, as described in section 3.2.b) and a large second, encrypted partition**
- **C.2 Split the space on SD-card between an unencrypted and an encrypted partition**
- **C.3 Create a small FAT32 partition (slightly below 8 GB, see section 3.2.b) and split the remaining space on SD-card between an unencrypted and an encrypted partition**