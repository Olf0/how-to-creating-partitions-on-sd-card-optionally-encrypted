These configuration files enable automated mounting of encrypted (partitions on) SD-card under SailfishOS 2.2.

After the design stabilised, a few weeks of testing, enhancing and optimising led to this set of configuration files, now being called "[crypto-sdcard](https://github.com/Olf0/crypto-sdcard/blob/master/README.md) 0.1 (2018-10-07)".

Create these files as root (i.e. with the usual ownerships and access rights: `root root -rw-r--r--`), with their contents as depicted below.<br />
*Note, that [archives (tar.gz / ZIP) are available](https://github.com/Olf0/crypto-sdcard/releases) of newer versions of these files, but they [require to rename the "key"-files](https://github.com/Olf0/crypto-sdcard/blob/master/README.md).*<br />
*Also note, that installable RPMs containing these newer versions are [available at OpenRepos](https://openrepos.net/content/olf/crypto-sdcard).* [TOC]
Then reboot and check as [described in section 2.3 of the second part](https://together.jolla.com/question/179060/how-to-externalising-android_storage-and-other-directories-files-to-sd-card/#179060-23-finishing-up), if working fine.<br />
You may also check the status of single units per `systemctl status -l "<unit-name>"` (with e.g. *crypto-sd-luks@mmcblk1p2.service* as unit name).<br />
The last paragraph of [section 4.3.5](https://together.jolla.com/question/179054/how-to-creating-partitions-on-sd-card-optionally-encrypted/#179054-43-dm-crypt-encrypted) (quick troubleshooting guide and superfluous messages in journal) is also applicable.

Check, if a link in /media/sdcard/ has been created:<br/>
`cd /data/media/`<br/>
`ls -l`<br />
If not, maybe a directory of the same name exists: Backup / (re)move it.

Please continue with [section 5](https://together.jolla.com/question/179054/how-to-creating-partitions-on-sd-card-optionally-encrypted/#179054-5-finishing-up).

<br />
Features this set of files provides:

* Start mounting encrypted (partitions on) SD-card via udisks at the earliest sensible time: Right after udisks2.service has started.
* Unmount before udisks2 begins stopping, hence achieving a clean unmount.
* Ensure, that AlienDalvik (alien-service-manager.service) begins starting after mounting succeeded, to allow for [android_storage on SD-card](https://together.jolla.com/question/179060/how-to-externalising-android_storage-and-other-directories-files-to-sd-card/#179060-2-externalising-homenemoandroid_storage).  Even more importantly this also ensures, that unmounting occurs only after AlienDalvik is completely stopped.<br />
Nevertheless, these configuration files are also applicable to devices without AlienDalvik installed.
* These configuration files do not alter, replace or delete any extant files.
* Boot time is not significantly prolonged, as unlocking encrypted partitions per Cryptsetup occurs in parallel to starting udisks2; after both succeeded, all mount operations are also started concurrently. 
* Create a "compatibility symlink" to allow older apps seamlessly accessing encrypted (partitions on) SD-cards at their new (since SailfishOS 2.2.0) mount point.

<br />
### /etc/udev/rules.d/82-crypto-sd.rules

    # For DM-Crypt LUKS, match sda0 to mmcblk1 to both SUBSYSTEM=="block" and ENV{ID_FS_TYPE}=="crypto_LUKS"
    KERNEL=="mmcblk1*|sd[a-z][0-9]", SUBSYSTEM=="block", ENV{ID_FS_TYPE}=="crypto_LUKS", ACTION=="add", MODE="0660", TAG+="systemd", ENV{SYSTEMD_WANTS}="crypto-sd-luks@%k.service"
    
    # For DM-Crypt "plain", also match sda0 to mmcblk1 to SUBSYSTEM=="block", but ensure (by ENV{ID_*}!= statements) that it appears to be unused space
    KERNEL=="mmcblk1*|sd[a-z][0-9]", SUBSYSTEM=="block", ENV{ID_FS_USAGE}!="filesystem", ENV{ID_FS_TYPE}!="crypto_LUKS", ENV{ID_PART_TABLE_TYPE}!="dos", ENV{ID_PART_TABLE_TYPE}!="gpt", ACTION=="add", MODE="0660", ENV{DM_CRYPT_PLAIN_SDCARD}="1", TAG+="systemd", ENV{SYSTEMD_WANTS}="crypto-sd-plain@%k.service"
    
    # Carefully match resulting virtual node dm-* to trigger mounting it; see /lib/udev/rules.d/10-dm.rules for details
    KERNEL=="dm-[0-9]*", SUBSYSTEM=="block", SYMLINK=="mapper/mmcblk1*|mapper/sd[a-z][0-9]", ENV{ID_FS_USAGE}=="filesystem", ENV{DM_UDEV_RULES_VSN}=="[1-9]*", ACTION=="change", ENV{DM_UDEV_PRIMARY_SOURCE_FLAG}=="1", ENV{DM_ACTIVATION}=="1", ENV{DM_SUSPENDED}=="0", MODE="0660", GROUP="disk", TAG+="systemd", ENV{SYSTEMD_WANTS}="crypto-sd-luks-udisks@%E{DM_NAME}.service"
    
    # Ditto for DM-Crypt "plain":
    KERNEL=="dm-[0-9]*", SUBSYSTEM=="block", SYMLINK=="mapper/mmcblk1*|mapper/sd[a-z][0-9]", ENV{ID_FS_USAGE}=="filesystem", ENV{DM_UDEV_RULES_VSN}=="[1-9]*", ACTION=="change", ENV{DM_UDEV_PRIMARY_SOURCE_FLAG}=="1", ENV{DM_ACTIVATION}=="1", ENV{DM_SUSPENDED}=="0", ENV{DM_CRYPT_PLAIN_SDCARD}=="1", MODE="0660", GROUP="disk", TAG+="systemd", ENV{SYSTEMD_WANTS}="crypto-sd-plain-udisks@%E{DM_NAME}.service"
    

### /etc/polkit-1/localauthority/50-local.d/69-crypto-sd-udisks.pkla

    [Allow nemo mounting encrypted SD-card]
    Identity=unix-group:system
    Action=org.freedesktop.udisks2.filesystem-mount-system
    ResultAny=yes
    ResultInactive=yes
    ResultActive=yes
    

### /etc/systemd/system/crypto-sd-luks@.service

    [Unit]
    Description=Open DM-Crypt LUKS on SD-card %i
    After=systemd-udevd.service systemd-udev-settle.service dev-%i.device
    BindsTo=dev-%i.device
    Conflicts=rescue.target actdead.target factory-test.target
    AssertFileNotEmpty=/etc/%i.key
    
    [Service]
    Type=oneshot
    RemainAfterExit=yes
    # Only needed on Jolla 1 phones running SailfishOS 2.2.0 for using XTS:
    # ExecStartPre=/sbin/modprobe qcrypto
    # For various reasons (dependency on udisks2, allow discards etc.), do not use "udisksctl unlock --key-file", call cryptsetup directly:
    ExecStart=/usr/sbin/cryptsetup --allow-discards -d /etc/%i.key luksOpen /dev/%i %i
    ExecStop=/usr/sbin/cryptsetup close %i
    

### /etc/systemd/system/crypto-sd-luks-udisks@.service

    [Unit]
    Description=Mount LUKS encrypted SD-card %i with udisks
    After=udisks2.service crypto-sd-luks@%i.service dev-mapper-%i.device
    BindsTo=udisks2.service crypto-sd-luks@%i.service dev-mapper-%i.device
    Wants=crypto-sd-symlink@%i.service
    Before=alien-service-manager.service
    
    [Service]
    User=nemo
    Type=oneshot
    RemainAfterExit=yes
    ExecStart=/usr/bin/udisksctl mount -b /dev/mapper/%i
    ExecStop=/usr/bin/udisksctl unmount -b /dev/mapper/%i
    

### /etc/systemd/system/crypto-sd-plain@.service

    [Unit]
    Description=Open DM-Crypt "plain" on SD-card %i
    After=systemd-udevd.service systemd-udev-settle.service dev-%i.device
    BindsTo=dev-%i.device
    Conflicts=rescue.target actdead.target factory-test.target
    AssertFileNotEmpty=/etc/%i.key
    
    [Service]
    Type=oneshot
    RemainAfterExit=yes
    # Only needed on Jolla 1 phones running SailfishOS 2.2.0 for using XTS:
    # ExecStartPre=/sbin/modprobe qcrypto
    ExecStart=/bin/sh -c 'cat /etc/%i.key | /usr/sbin/cryptsetup -h sha1 -s 256 -c aes-xts-plain --allow-discards --type plain open /dev/%i %i'
    ExecStop=/usr/sbin/cryptsetup close %i
    

### /etc/systemd/system/crypto-sd-plain-udisks@.service

    [Unit]
    Description=Mount "plain" encrypted SD-card %i with udisks
    After=udisks2.service crypto-sd-plain@%i.service dev-mapper-%i.device
    BindsTo=udisks2.service crypto-sd-plain@%i.service dev-mapper-%i.device
    Wants=crypto-sd-symlink@%i.service
    Before=alien-service-manager.service
    
    [Service]
    User=nemo
    Type=oneshot
    RemainAfterExit=yes
    ExecStart=/usr/bin/udisksctl mount -b /dev/mapper/%i
    ExecStop=/usr/bin/udisksctl unmount -b /dev/mapper/%i
    

### /etc/systemd/system/crypto-sd-symlink@.service

    [Unit]
    Description=Create media symlink to encrypted SD-card %i
    Documentation=https://github.com/Olf0/crypto-sdcard
    After=dev-mapper-%i.device
    Requisite=dev-mapper-%i.device
    
    [Service]
    # The symlink is meant to stay, so it cannot be accidentially overwritten.
    # OTOH, ln does not overwrite anything; it just fails, if a file, directory or link of that name already exists.
    ExecStart=-/bin/sh -c 'UUID="$(/sbin/blkid -c /dev/null -s UUID -o value /dev/mapper/%i)" && ln -s /run/media/nemo/$UUID /media/sdcard/$UUID'
